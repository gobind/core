/* globals describe, before, after, it */

'use strict';

describe('#auth-core', function() {
	var koa = require('koa-framework');
	var chai = require('chai');
	var nigah = require('nigah');
	var request = require('request');

	var expect = chai.expect;

	var db = require('gobind-db-nedb')();
	var auth = require('./../src')({ db: db });

	describe('#bcrypt', function() {
		it('should expose a hash function', function *() {
			var hash = yield auth.bcrypt.hashThunk('password', 1);
			expect(hash).to.be.a('string');
		});

		it('should expose a compare function', function *() {
			var hash = '$2a$04$byz4xAcDvat6CITc.6l70OKKGFVMkirJjfBn23cYmFSHvdgou.MOa';
			var match = yield auth.bcrypt.compareThunk('password', hash);
			expect(match).to.equal(true);
		});
	});

	describe('#randomToken', function() {
		it('should work without an explicit length', function *() {
			var token = yield auth.randomToken();
			expect(token).to.be.a('string');
		});

		it('should work with an explicit length', function *() {
			var token = yield auth.randomToken(8);
			expect(token).to.be.a('string').that.has.length(8);
		});
	});

	describe('#hook', function() {
		describe('#create', function() {
			it('should work', function *() {
				auth.hook.create(yield auth.randomToken());
			});

			it('should fail if it has already been created', function *() {
				var token = yield auth.randomToken();

				auth.hook.create(token);

				expect(function() {
					auth.hook.create(token);
				}).to.throw();
			});
		});

		describe('#register', function() {
			it('should fail if hook has not been created', function *() {
				var token = yield auth.randomToken();

				expect(function() {
					auth.hook.register(token);
				}).to.throw();
			});

			it('should fail if a generator is not passed', function *() {
				var token = yield auth.randomToken();

				// pre-condition
				auth.hook.create(token);

				// assertions
				expect(function() {
					auth.hook.register(token);
				}).to.throw();
				expect(function() {
					auth.hook.register(token, {});
				}).to.throw();
				expect(function() {
					auth.hook.register(token, function() {});
				}).to.throw();
			});

			it('should work', function *() {
				var token = yield auth.randomToken();

				auth.hook.create(token);
				auth.hook.register(token, function *() {}); // jshint ignore:line
			});
		});

		describe('#run', function() {
			var token1, token2;
			var assertionObject = {};
			var watcher = nigah(auth);

			before(function *() {
				token1 = yield auth.randomToken();
				token2 = yield auth.randomToken();

				auth.hook.create(token1);
				auth.hook.create(token2);

				auth.hook.register(token1, function *(ctx) {
					// assertion
					expect(ctx).to.have.property('context', true);

					assertionObject[1] = true;
				}); // jshint ignore:line
				auth.hook.register(token1, function *() {
					assertionObject[2] = true;
				}); // jshint ignore:line

				auth.hook.register(token2, function *() {
					assertionObject[3] = true;
				}); // jshint ignore:line
				auth.hook.register(token2, function *() {
					assertionObject[4] = true;
				}); // jshint ignore:line

				yield auth.hook.run(token1, { context: true });
			});

			after(function() {
				watcher.restore();
			});

			it('should call all relevant middleware', function() {
				expect(assertionObject).to.eql({ 1: true, 2: true });
			});

			it('should emit an event on auth emitter', function() {
				var count = {};
				count[token1] = 1;
				watcher.assertCount(count, true);
			});
		});
	});

	describe('#middleware', function() {
		describe('#loggedIn', function() {
			it('should return 401 if not authenticated', function(done) {
				var app = koa();
				app.env = 'test';
				app.use(auth.middleware.loggedIn);
				app.listen(3000);

				request('http://localhost:3000', function(err, res) {
					expect(res.statusCode).to.equal(401);
					done();
				});
			});

			it('should call next if authenticated', function(done) {
				var app = koa();
				app.env = 'test';

				// stub login middleware
				app.use(function *(next) {
					this.auth = { user: {} };
					yield next;
				});

				app.use(auth.middleware.loggedIn);

				// stub router
				app.use(function *() {
					this.status = 204;
				}); // jshint ignore:line

				app.listen(3001);

				request('http://localhost:3001', function(err, res) {
					expect(res.statusCode).to.equal(204);
					done();
				});
			});
		});
	});

	describe('#user', function() {
		describe('#create/insert', function() {
			it('should work', function *() {
				var user = yield auth.user.create({});
				expect(user).to.have.property('_id');
			});
		});

		describe('#findById', function() {
			var user;

			before(function *() {
				user = yield auth.user.create({});
			});

			it('should return nothing if user cant be found', function *() {
				var found = yield auth.user.findById('1234');
				expect(found).to.equal(null);
			});

			it('should return user if found', function *() {
				var found = yield auth.user.findById(user._id + '');
				expect(found).to.eql(user);
			});
		});

		describe('#deleteById/removeById', function() {
			var user;

			before(function *() {
				user = yield auth.user.create({});
			});

			it('should return false if user was not found', function *() {
				var removed = yield auth.user.removeById('1234');
				expect(removed).to.not.be.ok; // jshint ignore:line

				var found = yield auth.user.findById(user._id + '');
				expect(found).to.eql(user);
			});

			it('should return true if user was removed', function *() {
				var removed = yield auth.user.removeById(user._id + '');
				expect(removed).to.be.ok; // jshint ignore:line

				var found = yield auth.user.findById(user._id + '');
				expect(found).to.equal(null);
			});
		});

		describe('#updateById', function() {
			var user;

			before(function *() {
				user = yield auth.user.create({});
			});

			it('should return false if user was not found', function *() {
				var updated = yield auth.user.updateById('1234', {});
				expect(updated).to.not.be.ok; // jshint ignore:line

				var found = yield auth.user.findById(user._id + '');
				expect(found).to.eql(user);
			});

			it('should return true if user was updated', function *() {
				var updated = yield auth.user.updateById(user._id + '', {
					$set: { a: 1 }
				});
				expect(updated).to.be.ok; // jshint ignore:line

				var found = yield auth.user.findById(user._id + '');
				expect(found).to.eql({ _id: user._id, a: 1 });
			});
		});
	});
});