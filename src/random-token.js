'use strict';

const DEFAULT_LENGTH = 16;

var crypto = require('crypto');
var thunkify = require('thunkify');

var randomBytes = thunkify(crypto.randomBytes);

function *randomToken(length) {
	length = length || DEFAULT_LENGTH;

	var randomBuffer = yield randomBytes(length / 2);
	return randomBuffer.toString('hex');
}

module.exports = randomToken;