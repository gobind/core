'use strict';

/*
	Ensure user is logged in

	**Note:** How the user logged in is not covered by this middleware. It simply checks that the login strategy set the user correctly.
*/ 
exports.loggedIn = function *(next) {
	var auth = this.auth || {};
	
	this.assert(auth.user, 401);

	yield next;
};