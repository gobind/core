'use strict';

var thunkify = require('thunkify');

var bcrypt;
try {
	bcrypt = require('bcrypt');
} catch(e) {
	console.warn('bcrypt not found, using bcryptjs');
	bcrypt = require('bcryptjs');
}
['hash', 'compare', 'genSalt'].forEach(function(method) {
	bcrypt[method + 'Thunk'] = thunkify(bcrypt[method]);
});

module.exports = bcrypt;