'use strict';

var assert = require('assert');
var extend = require('extend');
var EventEmitter = require('eventemitter2').EventEmitter2;

var hook = require('./hook');
var user = require('./user');
var bcrypt = require('./bcrypt');
var middleware = require('./middleware');
var randomToken = require('./random-token');

module.exports = function(overrides) {
	var options = {
		emitter: { wildcard: false }
	};
	extend(options, overrides);

	assert(options.db, 'Missing database');

	var emitter = new EventEmitter(options.emitter);

	extend(emitter, {
		_gobindOptions: options,

		bcrypt: bcrypt,
		middleware: middleware,
		randomToken: randomToken
	});
	emitter.hook = hook(emitter, options);
	emitter.user = user(emitter, options);

	return emitter;
};