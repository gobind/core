/* jshint node:true */

'use strict';

function User(auth, options) {
	if (!(this instanceof User)) {
		return new User(auth, options);
	}

	this.auth = auth;
	this.options = options;
	this.db = options.db;

	this._createHooks();
}

User.hook = User.prototype.hook = {
	CREATE_BEFORE: 'user.create.before',
	CREATE: 'user.create',

	UPDATE_BY_ID_BEFORE: 'user.updateById.before',
	UPDATE_BY_ID: 'user.save',

	REMOVE_BY_ID_BEFORE: 'user.removeById.before',
	REMOVE_BY_ID: 'user.removeById'
};

User.prototype._createHooks = function() {
	for (var i in this.hook) {
		var hook = this.hook[i];

		this.auth.hook.create(hook);
	}
};

User.prototype.findById = function *(userId) {
	return yield this.db.findById(userId);
};

User.prototype.create = User.prototype.insert = function *(initialFields) {
	var user = initialFields || {};

	yield* this.auth.hook.run(this.hook.CREATE_BEFORE, { user: user });

	yield this.db.insert(user);

	yield* this.auth.hook.run(this.hook.CREATE, { user: user });

	return user;
};

User.prototype.deleteById = User.prototype.removeById = function *(userId) {
	var context = { userId: userId };

	yield* this.auth.hook.run(this.hook.REMOVE_BY_ID_BEFORE, context);

	var status = yield this.db.removeById(userId);
	context.status = status;

	yield* this.auth.hook.run(this.hook.REMOVE_BY_ID, context);

	return status;
};

User.prototype.updateById = function *(userId, update, options) {
	// some dbs error if passed undefined arguments
	options = options || {};

	var context = { userId: userId, update: update, options: options };

	yield* this.auth.hook.run(this.hook.UPDATE_BY_ID_BEFORE, context);

	var status = yield this.db.updateById(userId, update, options);
	context.status = status;

	yield* this.auth.hook.run(this.hook.UPDATE_BY_ID, context);

	return status;
};

module.exports = User;