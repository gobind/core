'use strict';

var assert = require('assert');

var slice = Array.prototype.slice;

function assertGenerator(fn) {
	assert.equal(typeof fn, 'function', 'Not a function');
	assert.equal(fn.constructor.name, 'GeneratorFunction', 'Not a generator function');
}

function Hook(auth) {
	if (!(this instanceof Hook)) {
		return new Hook(auth);
	}

	this.auth = auth;
	this._hooks = {};
}

Hook.prototype.hooks = function(name) {
	return this._hooks[name];
};

Hook.prototype.exists = function(name) {
	return name in this._hooks;
};

Hook.prototype.assertExists = function(name) {
	assert(this.exists(name), 'Hook not found');
};

// create a new hook namespace
Hook.prototype.create = function(name) {
	assert(!this.exists(name), 'Hook already exists');

	this._hooks[name] = [];
};

Hook.prototype.register = function(name, hook) {
	this.assertExists(name);
	assertGenerator(hook);

	this.hooks(name).push(hook);
};

Hook.prototype.run = function *(name, context) {
	this.assertExists(name);

	var args = slice.call(arguments);
	// remove name
	args.shift();

	// run through all the hooks sequentially
	var hooks = this.hooks(name);
	for (var i = 0, len = hooks.length; i < len; i += 1) {
		yield hooks[i](context);
	}

	// also emits an event on next tick
	setImmediate(function(auth) {
		auth.emit(name, context);
	}, this.auth);

	return context;
};

module.exports = Hook;